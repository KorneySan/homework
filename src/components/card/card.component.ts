import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'card-component',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})

export class CardComponent {
    @Input() cardText: string;
    @Output() textEmitter: EventEmitter<string> = new EventEmitter();
    public sendText(): void {
        this.textEmitter.next(this.cardText);
    }
}
