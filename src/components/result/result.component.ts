import { Component, Input } from '@angular/core';

@Component({
    selector: 'result-component',
    templateUrl: './result.component.html',
    styleUrls: ['./result.component.scss']
})

export class ResultComponent {
    //@Input() text: string;

    private result: string = '';

    public onTextEmitter2(text2: string): void {
      if (this.result=='') this.result = text2;
      else this.result += ' '+text2;
    }
}
