import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Home Work';

  public first_text: string = 'какой';
  public second_text: string = 'замечательный';
  public third_text: string = 'сегодня';
  public fourth_text: string = 'день';

  public divState: boolean = true;
  public changeDivState() {
    this.divState = !this.divState;
  }
  /* by HTML 
  public result: string = '';

  public sendText(text: string): void {
    if (this.result=='') this.result = text;
    else this.result += ' '+text;
  }
  */
  public onTextEmitter(text3: string): void {
  }
}
