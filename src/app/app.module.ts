import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ResultComponent } from '../components/result/result.component';
import { CardComponent } from '../components/card/card.component';

@NgModule({
  declarations: [
    AppComponent,
    ResultComponent,
    CardComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
